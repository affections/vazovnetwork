﻿using System.Linq;
using System.Windows.Forms;

namespace VazovNetwork
{
    public partial class AllCompetition : Form
    {
        public AllCompetition()
        {
            InitializeComponent();
        }
        public Db.SchoolDbContext _db = new Db.SchoolDbContext();
        private void refreshGrid()
        {

            var competitions = _db.Competitions.ToList();
            competitionsGrid.DataSource = competitions;
            competitionsGrid.Refresh();

        }

        private void AllCompetition_Load(object sender, System.EventArgs e)
        {
            refreshGrid();
        }
    }
}
