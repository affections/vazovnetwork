﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VazovNetwork
{
    public partial class AllSubjects : Form
    {
        public AllSubjects()
        {
            InitializeComponent();
        }
        public Db.SchoolDbContext _db = new Db.SchoolDbContext();
        private void refreshGrid()
        {

            var subjects = _db.Subjects.ToList();
            subjectsGrid.DataSource = subjects;
            subjectsGrid.Refresh();

        }

        private void subjectsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            refreshGrid();
        }
   
        private void AllSubjects_Load_1(object sender, EventArgs e)
        {
            refreshGrid();
        }
    }
}
